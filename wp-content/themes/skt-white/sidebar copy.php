<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package SKT White
 */
?>
<?php
                $args = array(
                    'post_type' => 'tribe_events',
                );
                $events = new WP_Query( $args );
                ?>
           
                <?php if ( $events->have_posts() ) : ?>
                    <?php while ( $events->have_posts() ) : $events->the_post(); ?>
                        <div class="panel panel-warning events_panel">
                            <div class="panel-heading events_panel_heading">
                                <h3 class="panel-title"><?php the_title(); ?></h3>
                            </div>
                            <div class="panel-body ">
                                <div class="row">
                                    <?php if( has_post_thumbnail() ):?>
                                    <div class="col-md-2">
                                    <?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
                                    <?php $post_thumbnail_url = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail', true ); ?>
                                    <img src="<?php echo $post_thumbnail_url[0]; ?>">
                                    </div>
                                    <?php endif; ?>
                                    <div class="col-md-8">
                                        <h4>
                                            <?php echo date('l, F j', strtotime(get_post_meta( $post->ID, '_EventStartDate',true )));?>
                                        </h4>
                                            <div class="time"><i class="fa fa-clock-o"></i> 
                                                <span class="date"><?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_EventStartDate',true )));?> -
                                                <span class="date"><?php echo date('g:i a', strtotime(get_post_meta( $post->ID, '_EventEndDate',true )));?></span></span>
                                            </div>
                                        
                                        <p class="entry-content tribe-events-event-entry"><?php //the_content(); ?></p>
                                        <!-- Event Meta -->
                                        <div class="tribe-events-event-meta vcard">
                                            <div class="author  location">
                                                            <!-- Venue Display Info -->
                                                <div class="tribe-events-venue-details" style="margin-top:10px; background-color: #fcf8e3; padding:7px">
                                                    <dl class="dl-vertical">
                                                        <dt>LOCATION :</dt>
                                                        <dd style="margin-bottom:10px;">
                                                            <i class="fa fa-location-arrow"></i><span class="author fn org"> <?php the_event_venue();?></span>
                                                        </dd>
                                                        <dt>ADDRESS :</dt>
                                                        <dd>
                                                            <i class="fa fa-map-marker"></i><?php echo tribe_the_full_address(); ?>
                                                        </dd>

                                                    </dl>
                                                    <a href="<?php echo tribe_get_map_link(); ?>" class="btn btn-default btn-sm" role="button">Google map</a>
                                                </div> <!-- .tribe-events-venue-details -->
                                                
                                            </div>
                                        </div><!-- .tribe-events-event-meta -->
                                    </div>
                                </div>
                            </div><!-- end events panel body-->
                        </div>
                    <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
            <?php endif;?>
            