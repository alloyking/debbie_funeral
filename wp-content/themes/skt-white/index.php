<?php
/**
 * The template for displaying home page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package SKT White
 */

get_header(); 
?>

<?php if ( 'page' == get_option( 'show_on_front' ) && ( '' != get_option( 'page_for_posts' ) ) && $wp_query->get_queried_object_id() == get_option( 'page_for_posts' ) ) : ?>

    <div class="content-area">
        <div class="middle-align content_sidebar">
            <div class="site-main" id="sitemain">
				<?php
                if ( have_posts() ) :
                    // Start the Loop.
                    while ( have_posts() ) : the_post();
                        /*
                         * Include the post format-specific template for the content. If you want to
                         * use this in a child theme, then include a file called called content-___.php
                         * (where ___ is the post format) and that will be used instead.
                         */
                        get_template_part( 'content', get_post_format() );
                
                    endwhile;
                    // Previous/next post navigation.
                    skt_white_pagination();
                
                else :
                    // If no content, include the "No posts found" template.
                     get_template_part( 'no-results', 'index' );
                
                endif;
                ?>
            </div>
            <?php get_sidebar();?>
            <div class="clear"></div>
        </div>
    </div>


<?php else: ?>
<section>
	<div class="container">
	    <div class="row">
	    	<div class="col-md-8">
			<div class="embed-responsive embed-responsive-16by9" style="margin-bottom:20px;">
				<iframe width="695" height="375" src="https://www.youtube.com/embed/_ngBGmLxgtM?wmode=transparent&amp;rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
		        <?php
		        $args = array(
					'post_type' => 'post',
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field'    => 'slug',
							'terms'    => array( 'journal', 'obit' ),
						),
					),
				);
				$journal = new WP_Query( $args );
		        ?>
		        <div class="service">
					<div class="panel panel-warning">
						<div class="panel-heading">
							<h3 class="panel-title">Journal Entries</h3>
						</div>
						<div class="panel-body">
							<?php if ( $journal->have_posts() ) : ?>
							<?php while ( $journal->have_posts() ) : $journal->the_post(); ?>
							<div class="col-md-2">
							<?php if( has_post_thumbnail() ):?>
							<?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
							<?php $post_thumbnail_url = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail', true ); ?>
							<img src="<?php echo $post_thumbnail_url[0]; ?>">
							
							<?php endif; ?>
							</div>
							<div class="col-md-9">
								<a href="<?php the_permalink();?>" class="journal">
									<div class="text" style="margin-bottom:20px">
								 		<h4><?php the_title();?></h4>
								 		<p class="small"><?php the_date();?></p>
								 		<p><?php echo wp_trim_words(get_the_content(), 50, '<a href="'. get_permalink() .'"> ...Read More</a>'); ?></p>
									</div>
								</a>
							</div>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							<?php endif; ?>
						</div>
					</div>
		        </div>
		   	</div>
		   	 <div class="col-md-4">
		   	 	 <?php get_sidebar('events');?>
		   	 </div>
	    </div>
	</div>
</section>

<?php endif; ?>


<?php get_footer(); ?>
