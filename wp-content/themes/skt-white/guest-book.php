<?php
/*
Template Name: Guest Book
*/
get_header(); ?>
<section>
<div class="container">
	<div class="row">
		<div class="col-md-8">
			<h2> Sign the GuestBook </h2>
			<?php gravity_form( 1, $display_title = false, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, $tabindex );?>
		</div>
		 <div class="col-md-4">
		   	 	 <?php get_sidebar('events');?>
		   	 </div>
	</div>
</div>
</section>
<?php get_footer(); ?>