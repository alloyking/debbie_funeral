<?php
/**
 * @package SKT White
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>
    
        <div class="col-md-12">
            <div class="tittle" style="margin-bottom:20px;"><h2><?php the_title();?></h2><small><?php the_date(); ?></small></div>
        <?php if (has_post_thumbnail() ) : ?>
            <div class="post-thumb">
                <?php the_post_thumbnail('medium'); ?>
            </div>
        <?php endif; ?>
        </div>
        <div class="col-md-8">
        
            <p><?php the_content(); ?></p>
        </div>
        <div class="col-md-4">
                 <?php get_sidebar('events');?>
             </div>
       
 
   

</article>