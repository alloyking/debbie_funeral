<?php
/*
Template Name: slideshow
*/
get_header(); ?>
<section>
<div class="container">
	<div class="row">
		<div class="col-md-8">
			<h2> Slideshow </h2>
			<p><iframe width="500" height="375" src="https://www.youtube.com/embed/_ngBGmLxgtM?wmode=transparent&amp;rel=0" frameborder="0" allowfullscreen></iframe></p>
		</div>
		 <div class="col-md-4">
		   	 	 <?php get_sidebar('events');?>
		   	 </div>
	</div>
</div>
</section>
<?php get_footer(); ?>
