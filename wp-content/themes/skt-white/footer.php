<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package SKT White
 */
?>

<?php if( is_home() || is_front_page() ) { ?>

<?php } ?>
 <div id="footer-wrapper">
    	<footer class="footer">
        	<div class="footer-col-1">
                
            	<h2>about debbie</h2>
                <p>Debbie was diagnosed with colon cancer in Sept. 2009 and had successful surgery to remove the tumor.  In Oct. 2009 we learned the cancer had spread through the lymph nodes to a few different spots.  The tumors are inoperable, but she's receiving chemo to control the cancer.  We are going to do our best to update so you know how to pray.</p>
            </div>
            
            <div class="footer-col-1">
            	<h2><?php echo of_get_option('recenttitle',__('Recent Posts','skt-white')); ?></h2>
                <ul class="recent-post">
                	<?php $query =  new wp_query(array('posts_per_page'   => 2)); ?>
                    <?php  while( $query->have_posts() ) : $query->the_post(); ?>
                  	<li><a href="<?php esc_url(the_permalink()); ?>"><?php get_the_post_thumbnail( get_the_ID(), array(67,49) ); ?><?php the_excerpt(); ?><br />
                    <span><?php _e('Read more...','skt-white'); ?></span></a></li>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </ul>
            </div>
            
            <div class="footer-col-3">
            	<h2>Social</h2>
                <a href="<?php echo esc_url('https://www.facebook.com/mark.pluim.7'); ?>"><div class="facebook icon"></div></a>
            </div>
            <div class="clear"></div>
        </footer>
        
        
    </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62104244-1', 'auto');
  ga('send', 'pageview');

</script>
  
<?php wp_footer(); ?>

</body>
</html>
